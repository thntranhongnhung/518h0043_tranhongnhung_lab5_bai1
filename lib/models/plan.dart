// import '../repositories/repository.dart';
import '../repositories/repository.dart';
import 'package:flutter/foundation.dart';
import 'task.dart';

class Plan {
  int id = 0;
  String name = '';
  List<Task> tasks = [];

  Plan({required this.id, this.name = ''});




  Model toModel() => Model(id: id, data: {
    'name': name,
    'tasks': tasks.map((task) => task.toModel()).toList()
  });

  int get completeCount => tasks.where((task) => task.complete).length;

  String get completenessMessage =>
      '$completeCount out of ${tasks.length} tasks';
}
